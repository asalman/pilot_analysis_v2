#include"tracking.h"
#include"ProcessTimeChecker.h"
#include<EdbSegP.h>
#include<EdbPattern.h>
#include<EdbDataSet.h>
#include<TCut.h>
#include<TStopwatch.h>
#include<TEnv.h>
#include<TThread.h>
#include<iostream>
#include<stdlib.h>
std::vector<Node*> nodes,jnodes;
std::vector<Link*> links;
hashtable3d* ht;
using namespace std;
float sigmaX2 = 2*2; // BT position resolution, 2 micron.
float sigmaY2 = 2*2; // BT position resolution, 2 micron.
float sigmaMTX = 0.3; // MT position resolution = 0.1 micron.
float sigmaMTZ = 5.0; // MT base thickness resolution = 2 micron.
float thicknessBase = 180.;
float sigmaT = sigmaMTX/thicknessBase;
float sigmaT2 = 0.000025 + sigmaT*sigmaT; // 5mrad arb
float sigmaMTZ2_thicknessBase2 = sigmaMTZ*sigmaMTZ/thicknessBase/thicknessBase;

// BT angular resolution is
// Transverse = sigmaMTX/thicknessBase*sqrt(2)
// Longitudinal = Transverse + tanTheta*sigmaMTZ/thicknessBase

// linking parameters
double linkingThreshold = 0.9999; // probability acceptance.
int linkingNdf = 4;
float linkingBranchAcceptance = 0.9; // provability to reject branches. TMath::ChisquareQuantile(linkingBranchAcceptance, linkingNdf)
int iplMax = -100000;
int iplMin =  100000;
float ztable[200];

class segnode : public Node{
	public:
	
	segnode(EdbSegP* s) {
		v[0] = s->Plate();
		v[1] = s->X();
		v[2] = s->Y();
		data = new EdbSegP(*s);
	};
	EdbSegP* getData() { return (EdbSegP*) data;}
	virtual int ID() { return ((EdbSegP*) data)->ID();}
};

void read_nodes_tree(char* filename) {
	TFile f(filename);
	printf("Read %s\n", filename);
	for (int i = 0; i < 29; i++) {
		TTree *t = (TTree*)f.Get(Form("tree%d",i+90));
		Float_t x,y,z,tx,ty,etruth;
		Int_t iz,id,pdgid,clstruth;
		iz = i+90;
		t->SetBranchAddress("x",&x);
		t->SetBranchAddress("y",&y);
		t->SetBranchAddress("z",&z);
		t->SetBranchAddress("tx",&tx);
		t->SetBranchAddress("ty",&ty);
//		t->SetBranchAddress("iz",&iz);
		t->SetBranchAddress("id",&id);
		t->SetBranchAddress("pdgid",&pdgid);
		t->SetBranchAddress("clstruth",&clstruth);
		t->SetBranchAddress("etruth",&etruth);
		for (int j = 0, patN = t->GetEntries(); j < patN; j++) {
			if(j%100000==0)printf("%d / %d\n",j,patN);
			t->GetEntry(j);
			if(etruth<20)continue;
//			if(x-62500<-20500||x-62500>-19500)continue;
//			if(y-50000<-9000||y-50000>-8000)continue;
			EdbSegP* s = new EdbSegP;
			s->SetX(x);
			s->SetY(y);
			s->SetZ(z);
			s->SetTX(tx);
			s->SetTY(ty);
			s->SetPlate(i+90);
			s->SetID(id);
			s->SetMC(clstruth,pdgid);
			s->SetP(etruth);
			segnode* sn = new segnode(s);
			nodes.push_back(sn);
		}
	}
}
void GhostRejection(std::vector<Node *>& nodes){
	
	// Fill hash table; /////////////
 	// find data range
 	int iplMin=1000000, iplMax=-1000000;
	float xmin =  1e9;
	float xmax = -1e9;
	float ymin =  1e9;
	float ymax = -1e9;
	for (int i = 0, nodes_size = nodes.size(); i < nodes_size; i++) {
		EdbSegP* s = (EdbSegP*) nodes[i]->getData();
		int sPlate = s->Plate();
		float sX = s->X();
		float sY = s->Y();
		if (iplMin > sPlate) iplMin = sPlate;
		if (iplMax < sPlate) iplMax = sPlate;
		if (xmin > sX) xmin = sX;
		if (xmax < sX) xmax = sX;
		if (ymin > sY) ymin = sY;
		if (ymax < sY) ymax = sY;
	}
	xmin -= 1; xmax += 1; ymin -= 1; ymax += 1;
	printf("ipl %d - %d\n", iplMin, iplMax);
	printf("range (%.0f, %.0f) - (%.0f, %.0f)\n", xmin, ymin, xmax, ymax);
	
	
	// use of adhoc 3D hastable written by Aki for tracking. (in traking.h)
	hashtable3d ht;
	
	// initialize the hashtable
	float cellsize = 50; // 50 micron cell size
	ht.setCells(iplMax - iplMin + 1, iplMin - 0.5, iplMax + 0.5,
	             floor((xmax - xmin)/cellsize) + 1, xmin, xmax,
	             floor((ymax - ymin)/cellsize) + 1, ymin, ymax);
	// fill hash table
	ht.fillCells(nodes);
	// Ghost rejection ////////////////////////////
	// definition of radious.
	float r[3] = {1., 100, 100}; // FIXME - suppos to be variable wrt BT slope?
	// parameters from Yoshimoto's presentation but mm->mkm
	double th_pos = 2.25;				//[mkm]
	double th_angle = 3.6;				//[mkm]
	double rate_pos_radial = 0.1;
	double rate_pos_lateral = 0;
	double rate_angle_radial = 0.35;
	double rate_angle_lateral = 0;
	double th_pos_radial_max = 9;		//[mkm]
	double th_pos_lateral_max = 2.25;	//[mkm]
	double th_angle_radial_max = 24.3;	//[mkm]
	double th_angle_lateral_max = 3.6;	//[mkm]
    //fill with MT positions, slopes 
	double vx1, vy1, vx2, vy2;
	double rotation_cos, rotation_sin; 
	double tr1, tl1,  tr2, tl2, length, dcr[2], dcl[2];
	double th_dcr, th_dcl, th_dtr, th_dtl;
	double mt1pos[2][2], mt2pos[2][2];// the same for BT and both MTs, no smearing is applied
	int mt1ph[2], mt2ph[2]; // [0] - bottom with smaller z, [1] - top with larger z
	double base = 210., emu = 70.; // 2018 geometry
	double dx[2], dy[2];
	
	// loop over all segments.
	printf("Ghost rejection\n");
	for(int i=0; i<nodes.size(); i++){//loop on all
		if(i%10000==0)printf("%d / %d\n",i,nodes.size());
		EdbSegP *s1 = ((segnode *)nodes[i])->getData();
//		printf("BT 1: %f %f %f %f %f %d\n",s1->X(),s1->Y(),s1->Z(),s1->TX(),s1->TY(),s1->Plate());
		if(s1->Flag()) continue;
		float v[3];
		v[0] = s1->Plate();
		v[1] = s1->X();
		v[2] = s1->Y();
		
		// mt parameters
		mt1pos[0][0] = s1->X() - s1->TX()*(base+emu)/2.;// x bottom
		mt1pos[1][0] = s1->Y() - s1->TY()*(base+emu)/2.;// y bottom
		
		mt1pos[0][1] = s1->X() + s1->TX()*(base+emu)/2.;// x top
		mt1pos[1][1] = s1->Y() + s1->TY()*(base+emu)/2.;// y top
		
		mt1ph[0] = gRandom->Binomial(s1->W(), s1->W()*0.5); // randomize from the sum.
        mt1ph[1] = s1->W()-mt1ph[0];
        
		// tx,ty -> cosx*length, cosy*length
		vx1 = s1->TX()*emu;
		vy1 = s1->TY()*emu;
		
		rotation_cos = cos(-atan2(vy1, vx1));
		rotation_sin  = sin(-atan2(vy1, vx1));
		
		length = abs(vx1 * rotation_cos - vy1 * rotation_sin);
		tr1 = vx1 * rotation_cos - vy1 * rotation_sin;
		tl1 = vx1 * rotation_sin + vy1 * rotation_cos;

		th_dcr = std::min(th_pos + length * rate_pos_radial, th_pos_radial_max);
		th_dcl = std::min(th_pos + length * rate_pos_lateral, th_pos_lateral_max);
		th_dtr = std::min(th_angle + length * rate_angle_radial, th_angle_radial_max);
		th_dtl = std::min(th_angle + length * rate_angle_lateral, th_angle_lateral_max);
		
		 
		// find neighboring nodes with radious r.
		std::vector<Node*>& neighbors = ht.getNeighbors(v, r);
		int nn = neighbors.size();
			
		for (int j = 0; j < nn; j++) {
			segnode* n2 = (segnode *) neighbors[j];
			EdbSegP *s2 = n2->getData();
			// if two segments are on different plate, continue
			if(s1->Plate()!=s2->Plate()) continue; 
			// if two segments are the same, continue;
			if(s1 == s2) continue;
			// if s2 is already rejected, continue;
			if(s2->Flag()) continue;
			// Yoshimoto's condition is to be implemented here.
			// mt parameters
//			printf("BT 2: %f %f %f %f %f %d\n",s2->X(),s2->Y(),s2->Z(),s2->TX(),s2->TY(),s2->Plate());
		   mt2pos[0][0] = s2->X() - s2->TX()*(base+emu)/2.;// x bottom
		   mt2pos[1][0] = s2->Y() - s2->TY()*(base+emu)/2.;// y bottom
		
		   mt2pos[0][1] = s2->X() + s2->TX()*(base+emu)/2.;// x top
		   mt2pos[1][1] = s2->Y() + s2->TY()*(base+emu)/2.;// y top
		
		   mt2ph[0] = gRandom->Binomial(s2->W(), s2->W()*0.5); // randomize from the sum.
           mt2ph[1] = s2->W()-mt2ph[0];
        
		   vx2 = s2->TX()*emu;
		   vy2 = s2->TY()*emu;
		   
		    tr2 = vx2 * rotation_cos - vy2 * rotation_sin;
		    tl2 = vx2 * rotation_sin + vy2 * rotation_cos;
//		printf("j = %d,dtr = %f,dtl = %f\n",j,tr2-tr1,tl2-tl1);
			for(int imt=0;imt<2;imt++){
				dx[imt] = mt2pos[0][imt] -  mt1pos[0][imt];
				dy[imt] = mt2pos[1][imt] -  mt1pos[1][imt];
				
				dcr[imt] = dx[imt]  * rotation_cos - dy[imt]  * rotation_sin;
				dcl[imt] = dx[imt]  * rotation_sin + dy[imt]  * rotation_cos;
//				printf("dcr = %lf, dcl = %lf\n",dcr[imt],dcl[imt]);
				if(abs(dcr[imt])<=th_dcr && abs(dcl[imt]) <= th_dcl && abs(tr2 - tr1) <= th_dtr && abs(tl2 - tl1) <= th_dtl){
					// set flags
					if(mt1ph[imt] >= mt2ph[imt] ){
						s2->SetFlag(1);
					}
					else {
						s1->SetFlag(1);
					}
//					printf("j = %d,imt = %d,dtr = %f,dtl = %f\n",j,imt,tr2-tr1,tl2-tl1);
//					printf("dcr = %lf %lf (threshold %lf)\n",abs(dcr[imt]),dcr[imt],th_dcr);
//					printf("dcl = %lf (threshold %lf)\n",abs(dcl[imt]),th_dcl);
//					printf("dtr = %lf (threshold %lf)\n",abs(tr2-tr1),th_dtr);
//					printf("dtl = %lf (threshold %lf)\nREJECTED\n",abs(tl2-tl1),th_dtl);
//					cout << "dcr[imt] debug = " << dcr[imt] << " " << abs(dcr[imt]) << " " << th_dcr << endl;

				}
			}
			if(s1->Flag() ==1 ) break;// break the for loop of n2. and go to next nodes[i].
		} // loop on neighbors
	} // loop on all
}

void write_tracks() {
	
	printf("Write tracks into file.\n");
	std::vector<Node*> nodes;
	hashtable3d ht;	
	TFile f("linked_tracks.root", "recreate");
	TTree* tracks = new TTree("tracks", "tracks");

	EdbTrackP*    track = new EdbTrackP(8);
	EdbSegP*      tr = new EdbSegP;
	TClonesArray* segments  = new TClonesArray("EdbSegP");
	TClonesArray* segmentsf = new TClonesArray("EdbSegP");

	int   nseg, trid, npl, n0;
	float w = 0.;
	float xv, yv;

	tracks->Branch("trid", &trid,     "trid/I");
	tracks->Branch("nseg", &nseg,     "nseg/I");
	tracks->Branch("npl",  &npl,      "npl/I");
	tracks->Branch("n0",   &n0,       "n0/I");
	tracks->Branch("xv",   &xv,       "xv/F");
	tracks->Branch("yv",   &yv,       "yv/F");
	tracks->Branch("w",    &w,        "w/F");
	tracks->Branch("t.",   "EdbSegP", &tr, 32000, 99);
	tracks->Branch("s",    &segments);
	tracks->Branch("sf",   &segmentsf);
	
	int itrk = 0;
	for (int i = 0, links_size = links.size(); i < links_size; i++) {
		Link* l = links[i];
		if (l->flag == Link::REJECTED) continue;
		
		EdbSegP* s1 = (EdbSegP*) l->nodeFirst()->getData();
		EdbSegP* s2 = (EdbSegP*) l->nodeLast()->getData();
		// tr = s1; //  <-- source of bug!! 2018/5/14
		tr->SetID(itrk);
		
		*tr = *s1;
		
		trid = tr->ID();
		nseg = l->nodes.size();
		npl  = s2->Plate() - s1->Plate() + 1;
		n0   = npl - nseg;
		
		segments->Clear("C");
		segmentsf->Clear("C");
		
		w = 0;
		for (int j = 0; j < nseg; j++) {
			w += ((EdbSegP*) l->nodes[j]->getData())->W();
		}
		
		EdbSegP *s = 0, *sf = 0;
		for (int is = 0; is < nseg; is++) {
			s = (EdbSegP*) l->nodes[is]->getData();
			if (s) new((*segments)[is]) EdbSegP(*s);
			sf = s;
			if (sf) new((*segmentsf)[is]) EdbSegP(*sf);
		}

		tr->SetVid(0, itrk);  // put track counter in t.eVid[1]
		itrk++;
		tracks->Fill();
	}
	
	tracks->Write();
	f.Close();
}
int main(int argc, char* argv[]) {
	
	if (argc < 2) return 1;
	
	char* filename = argv[1];
//	TCut cut = "1";
	
	
	ProcessTimeChecker pc;
	pc.AddEntry("Read file");

	TString fname = filename;
	if (fname.Contains(".root")) {
		read_nodes_tree(filename);
	}
	else return 1;	
	GhostRejection(nodes);
	int nAlive=0;
	for(int i=0; i<nodes.size(); i++){
		if( ((segnode *) nodes[i])->getData()->Flag() ==0){
			nAlive++;
			jnodes.push_back(nodes[i]);
		}
	}
	printf("%d segments alived after Ghost Rejection\n", nAlive);
	printf("jnodes.size() = %d\n",jnodes.size());
	TFile g(argv[2],"RECREATE");
	TTree *tw[29];
	Float_t x,y,z,tx,ty,etruth;
	Int_t iz,id,pdgid,clstruth;
	for(int i=0;i<29;i++){
		tw[i] = new TTree(Form("tree%d",i+90),Form("BTs in layer %d",i+90));
		tw[i]->Branch("x",&x,"x/F");
		tw[i]->Branch("y",&y,"y/F");
		tw[i]->Branch("z",&z,"z/F");
		tw[i]->Branch("tx",&tx,"tx/F");
		tw[i]->Branch("ty",&ty,"ty/F");
		tw[i]->Branch("id",&id,"id/I");
		tw[i]->Branch("pdgid",&pdgid,"pdgid/I");
		tw[i]->Branch("clstruth",&clstruth,"clstruth/I");
		tw[i]->Branch("etruth",&etruth,"etruth/F");
	}
	for(int i=0;i<jnodes.size();i++){
//		flag = ((segnode*)nodes[i])->getData()->Flag();
//		if(flag)continue;
		x = ((segnode*)jnodes[i])->getData()->X();
		y = ((segnode*)jnodes[i])->getData()->Y();
		z = ((segnode*)jnodes[i])->getData()->Z();
		tx = ((segnode*)jnodes[i])->getData()->TX();
		ty = ((segnode*)jnodes[i])->getData()->TY();
		iz = ((segnode*)jnodes[i])->getData()->Plate();
		id = ((segnode*)jnodes[i])->getData()->ID();
		etruth = ((segnode*)jnodes[i])->getData()->P();
		pdgid = ((segnode*)jnodes[i])->getData()->MCTrack();
		clstruth = ((segnode*)jnodes[i])->getData()->MCEvt();
		tw[iz-90]->Fill();
		if(i%100000==0)printf("%d / %d %f %f %f %f %f %d\n",i,jnodes.size(),x,y,z,tx,ty,iz,id);
	}
	for(int i=0;i<29;i++)tw[i]->Write();
	g.Close();
	return 0;
}

